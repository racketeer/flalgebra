#lang racket

(module configuration racket/base
  (provide N)
  (define N 1000000))

(require 'configuration)
(displayln (~a "# " N " iterations"))
(newline)

(module flalgebra-test racket/base

  (require "flalgebra.rkt"
           (submod ".." configuration))

  (provide run-flalgebra-test)

  (define-flalgebra 3 vec mat)
  

  (define MR (mat3x3/0))
  (define M1 (mat3x3 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0))
  (define M2 (mat3x3 9.0 8.0 7.0 6.0 5.0 4.0 3.0 2.0 1.0))
  (define VR (vec3/0))
  (define V1 (vec3 1.0 2.0 3.0))
  (define V2 (vec3 2.0 3.0 4.0))
  (define f 12.34)

  (define (run-flalgebra-test)
    (displayln "## flalgebra")
    (newline)

    (display "mat3x3*mat3x3! ")
    (time
     (for ((i (in-range N)))
       (mat3x3*mat3x3! MR M1 M2)))

    (display "mat3x3*mat3x3 ")
    (time
     (for ((i (in-range N)))
       (mat3x3*mat3x3 M1 M2)))

    #;(
    (display "mat3x3*vec3! ")
    (time
     (for ((i (in-range N)))
       (mat3x3*vec3! VR M1 V1)))

    (display "mat3x3*vec3 ")
    (time
     (for ((i (in-range N)))
       (mat3x3*vec3 M1 V1)))

    (display "vec3+vec3! ")
    (time
     (for ((i (in-range N)))
       (vec3+vec3! VR V1 V2)))

    (display "vec3+vec3 ")
    (time
     (for ((i (in-range N)))
       (vec3+vec3 V1 V2)))

    (display "vec3*fl3! ")
    (time
     (for ((i (in-range N)))
       (vec3*fl! VR V1 f)))

    (display "vec3*fl3 ")
    (time
     (for ((i (in-range N)))
       (vec3*fl V1 f)))

    (display "vec3.vec3 ")
    (time
     (for ((i (in-range N)))
       (vec3.vec3 V1 V2))))

    (newline)
    ))

(module math-matrix-test racket/base

  (provide run-math-matrix-test)

  (require (submod ".." configuration)
           math/matrix)

  (define (run-math-matrix-test)

    (displayln "## math/matrix")
    (newline)

    (define M1 (matrix ((1.0 2.0 3.0)
                        (4.0 5.0 6.0)
                        (7.0 8.0 9.0))))
    (define M2 (matrix ((9.0 8.0 7.0)
                        (6.0 5.0 4.0)
                        (3.0 2.0 1.0))))
    (define V1 (matrix ((1.0) (2.0) (3.0))))
    (define V2 (matrix ((2.0) (3.0) (4.0))))

    (display "matrix*matrix ")
    (time
     (for ((i (in-range N)))
       (matrix* M1 M2)))

    (newline)
    ))

(module typed-math-matrix-test typed/racket

  (provide run-typed-math-matrix-test)

  (require/typed (submod ".." configuration)
    (N Natural))
  (require math/matrix)

  (define (run-typed-math-matrix-test)
    (displayln "## math/typed/matrix")
    (newline)

    (define M1 (matrix ((1.0 2.0 3.0)
                        (4.0 5.0 6.0)
                        (7.0 8.0 9.0))))
    (define M2 (matrix ((9.0 8.0 7.0)
                        (6.0 5.0 4.0)
                        (3.0 2.0 1.0))))
    (define V1 (matrix ((1.0) (2.0) (3.0))))
    (define V2 (matrix ((2.0) (3.0) (4.0))))

    (display "matrix*matrix ")
    (time
     (for ((i (in-range N)))
       (matrix* M1 M2)))

    (newline)))

(module flomat-test racket/base

  (require (submod ".." configuration)
           flomat)

  (provide run-flomat-test)

  (define (run-flomat-test)

    (displayln "## flomat")
    (newline)
    
    (define M1 (matrix '((1.0 2.0 3.0)
                        (4.0 5.0 6.0)
                        (7.0 8.0 9.0))))
    (define M2 (matrix '((9.0 8.0 7.0)
                        (6.0 5.0 4.0)
                        (3.0 2.0 1.0))))
    (define V1 (matrix '((1.0) (2.0) (3.0))))
    (define V2 (matrix '((2.0) (3.0) (4.0))))

    (collect-garbage)
    (display "matrix*matrix! ")
    (time
     (for ((i (in-range N)))
       (times! M1 M2)))
    
    (collect-garbage)
    (display "matrix*matrix ")
    (time
     (for ((i (in-range N)))
       (times M1 M2)))
    
    (newline)))

(require 'flalgebra-test)
(run-flalgebra-test)

(require 'math-matrix-test)
(run-math-matrix-test) ; 10x slower than typed variant

(require 'typed-math-matrix-test)
(run-typed-math-matrix-test)

(require 'flomat-test)
(run-flomat-test)
